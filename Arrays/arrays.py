def array():
	array = [1,2,3,5,8]
	array.pop()
	array.append(13)
	print(array)
	print("Index of 2: ",array.index(2))
	array.remove(1)
	print("Removed 1:",array)
	
	array.reverse()
	print("reversed: ",array)
	print("Sorting array:  ",sorted(array))
	print("print last 3: ",array[:3])
	#sorting back
	array.sort()
	print("sorted in place: ",array)
	#add charachters to array from a to z
	for i in range(65,91):
		array.append(chr(i))
	print(array)

	print("print only characters: ",)
	array2="".join(str(i) for i in array)
	array2=list(array2)
	print(array2)	
	alphas=[]
	for i in array2:
		if i.isalpha():
			alphas.append(i)
	print(alphas)
	
	

def main():
	array()

if __name__ == "__main__":
	main()
