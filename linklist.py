#access an element O(N)
#add/remove element at an iterator postion O(1)
#Remove last element O(n)

#this class will create nodes in the linklist with data
class node:
	def __init__(self,data): #data=None can be initilaized here too
		self.data = data;
		self.next = None;

class linklist:
	def __init__(self):
		self.head = node(None) #data has to be none at the start
	#this will create the nodes		
	def append(self,data):
		new_node = node(data);
		cur = self.head;  #this is initializer of the holder
		while cur.next!=None:
			cur = cur.next;
		cur.next = new_node;

	#calculating length
	def length(self):
		cur = self.head;
		total = 0;
		while cur.next!=None:
			total+=1;
			cur = cur.next;
		return total;


	#prints out the list
	def display(self):
		elements = []
		cur = self.head;
		while cur.next!=None:
			cur=cur.next
			elements.append(cur.data)
		print(elements)

	
	#can check what element is at index 
	def get(self,index):
		if index>=self.length():
			print("error")
		cur_idx=0
		cur=self.head;
		while True:
			cur=cur.next;
			if cur_idx == index:
				return cur.data
			cur_idx+=1

	#can remove using the index
	def remover(self,index):
		if index>=self.length():
			print("error");
			return;
		ind=0;
		cur=self.head;
		while cur.next!=None:
			last_node = cur;
			cur=cur.next;
			if index == ind:		
				last_node.next = cur.next
				return
			ind+=1

	#can search the data available
	def search(self,input):
		cur = self.head;
		index = 0;
		while cur.next!=None:
			cur = cur.next;
			if input == cur.data:
				print("exist at index:%d" %index);
				return;		
			elif(cur.next==None):
				print("not found")
			index+=1;
		

#object created
link=linklist()

#adding alphabets to the list
for i in range(97,123):
	link.append(chr(i))
link.append("this is sonic")

#displays
link.display()
#printing a value from the index
print(link.get(2))

#removing a list using index
link.remover(0)
link.display()

#searching from list
link.search("z")

