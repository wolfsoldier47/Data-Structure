public class Linklist
{

  private Node head;  //private functions for accessing head
  private Node tail;  //private functions for accesssing tail
									  //All from node function
 
  public Linklist() 
  {
    head = null;
    tail = null;
  }


  public void traverselist() 
  {
    Node current = head;   //selecting the header
    Node previous = null;  //selecting the end point
    while(current != null)  // traversing throught the list till the end or NULL
	{
      System.out.print(current.getValue() + " "); //printing out the values
      previous = current;   //changing the node address to the crrent one
      current = current.next; //going to next node
    }
  }  

  public void addFirst(Node newNode) 
  {
    if (newNode == null)   //if empty, nope
	{
  	  return;    
  	} 
	else 
	{
  		if (head == null)   //adding if the head is null
		{
  			newNode.next = null;
  			head = newNode;
  			tail = newNode;
  		}
		 else 
		{
  			newNode.next = head;
  			head = newNode;
  		}
  	}
  }

  public void insertAfter(Node previous,Node newNode) //adding with the movement
  {
  	if (newNode == null)
 	{
  		return;
  	}
     else 
     {
  		if (previous == null) 
		{
  			addFirst(newNode);
  		} 
		else if (previous == tail) 
		{
  			addLast(newNode);
  		}
		 else 
		{
  			Node next = previous.next;
  			previous.next = newNode;
  			newNode.next = next;
  		}
  	}
  }

  public void addLast(Node newNode) //adding at the last
  {
    if (newNode == null) 
    {
      return;
    }
	 else 
    {
      newNode.next = null;  
      if (head == null) 
      {
        head = newNode;
        tail = newNode;
      } else {
        tail.next = newNode;
        tail = newNode;
      }
    }
  }

  public void removeFirst() //Remove from the beginning
  {
  	if (head == null) 
    {
  	  return;
  	} else 
    {
  		if (head == tail) 
        {
  			head = null;
  			tail = null;
  		} else 
        {
  			head = head.next;
  		}
  	}
  }

  public void removeLast() //Remove from the last
  {
  	if (tail == null) 
    {
  		return;
  	} 
    else 
    {
  		if (head == tail) 
        {
  			head = null;
  			tail = null;
  		} 
        else 
		{
  			Node prevsToTail = head;
  			while (prevsToTail.next != tail) 
            {
  				prevsToTail = prevsToTail.next;
  			}
  			tail = prevsToTail;
  			tail.next = null;
  		}
  	}
  }

  public void removeNext(Node prevs) 
  {
  	if (prevs == null) 
    {
  		removeFirst();
  	} else if (prevs.next == tail) 
    {
  		tail = prevs;
  		tail.next = null;
  	} else if (prevs == tail) 
    {
  		return;
  	} else 
    {
  		prevs.next = prevs.next.next;
  	}
  }

  public static void main(String[] args) 
  {
    Linklist list = new Linklist(); //creating instance
    Node data1 = new Node(99); //node instance with data
    Node data2 = new Node(1001);
    Node data3 = new Node(151);
    Node data4 = new Node(1091);
    list.addFirst(data1);
    list.insertAfter(data1,data2);//inserting after data1
    list.insertAfter(data2,data3);//inserting after data2
    list.addLast(data4); //adding to last
    System.out.print("print the list: ");
    list.traverselist(); 
    System.out.println();
  }

}
